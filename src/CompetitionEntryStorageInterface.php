<?php

namespace Drupal\competition;

use Drupal\Core\Entity\ContentEntityStorageInterface;

/**
 * Defines an interface for competition_entry entity storage classes.
 */
interface CompetitionEntryStorageInterface extends ContentEntityStorageInterface {

}
